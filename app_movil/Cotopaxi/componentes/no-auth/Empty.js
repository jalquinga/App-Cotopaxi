import React from 'react' ;
import {
    View,
    Text,
    StyleSheet, ImageBackground, Dimensions, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const {width: WIDTH} = Dimensions.get('window');
function  Empty(props) {
    //console.log('ok');
    return(



        <View style={styles.container}>

            <View>
                <Text style={styles.statusPedido}>Status de tu pedido:</Text>
            </View>
            <View style={styles.containerStatus}>
                <Icon  name="ios-settings" size={40} color="#cc3906" />
                <Text style={styles.containerStatusText}>PRODUCCIÓN</Text>
            </View>

            <Icon style={styles.containerStatusIco} name="ios-arrow-round-down"   />


            <View style={styles.containerStatus}>
                <Icon name="ios-cube" size={40} color="#f5c15d" />
                <Text style={styles.containerStatusText}>EMBALAJE</Text>
            </View>

            <Icon style={styles.containerStatusIco} name="ios-arrow-round-down"   />

            <View style={styles.containerStatus}>
                <Icon name="ios-home" size={40} color="#77b5fa" />
                <Text style={styles.containerStatusText}>BODEGA</Text>
            </View>

            <Icon style={styles.containerStatusIco} name="ios-arrow-round-down"   />

            <View style={styles.containerStatus}>
                <Icon name="ios-paper" size={40} color="#9dc94e" />
                <Text style={styles.containerStatusText}>FACTURADO</Text>
            </View>


        </View>




    )

}


const styles = StyleSheet.create({
    containerStatus:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 30,
       // justifyContent: 'center',
    },
    containerStatusText:{
        marginLeft:15,
        fontSize: 15,
        fontWeight:'bold',
        color:'#a7a7a7'

    },
    containerStatusIco:{
        marginLeft: 43,
        color:'#f06c26',
        fontSize: 22
    },
    statusPedido:{
        fontSize: 18,
        paddingBottom: 15,
        color:'#a7a7a7',
        textAlign:'center'
    },
    container:{
        marginTop: 10,

        flex: 1,
        height: 400,
    },
    text: {
        color: '#f5a128',
        fontSize: 20,
        textAlign: 'center',

    },
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center',



        //justifyContent: 'center',
        //alignItems: 'center',

    },
})

export default Empty;
