import React from 'react';

import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';


function SuggestionListLayout(props){
    return(
        <View style={styles.container}>
            <ScrollView >
                {props.children}
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'column'

    },
    title: {
        color: '#4c4c4c',
        fontSize: 20,
        marginBottom: 10,
        fontWeight: 'bold',
    }
})


export default SuggestionListLayout;
