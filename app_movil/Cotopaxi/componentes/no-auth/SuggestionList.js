import React, {Component} from 'react';
import {ScrollView, Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {withNavigation} from 'react-navigation';
import Layout from './SuggestionListLayout';
import Empty from './Empty';
import Separator from './VerticalSeparator'
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';

class SuggestionList extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible, item) {
        this.setState({
            modalVisible: visible,
            itemTRDOCO: item.TRDOCO,
            itemTRVR01: item.TRVR01,
            itemTRAN8: item.TRAN8,
            itemTRUORG: item.TRUORG / 10000,
            itemTRDSC1: item.TRDSC1,
            itemTRDSC2: item.TRDSC2,
            itemTRDC20: item.TRDC20.trim(),
            itemTRPDDJ: item.deliveryDate,
            itemTRIVD: item.billingDate,

        });

    }

    changeModalVisibility = (modalVisible = false) => {

        this.setState({modalVisible});

    };


    keyExtractor = item => JSON.stringify(item);
    renderEmpty = () => <Empty/>;
    itemSeparator = () => <Separator/>;
    renderItem = ({item}) => {

        const julianDate = (julian, format) => {
            format = format || 'DD/MM/YYYY';

            // Convert number to string so that we can split the digits
            const julianDate = (julian + "").split('');
            const first = (19 + parseInt(julianDate[0])) * 100;
            const second = julianDate[1] + julianDate[2];
            const year = first + parseInt(second);
            const days = julianDate[3] + julianDate[4] + julianDate[5];

            return moment([year, 0, 1]).add(parseInt(days) - 1, 'days').format(format);
        };

        const handlePress = () => {
            item.deliveryDate = julianDate(item.TRPDDJ);
            item.billingDate = julianDate(item.TRIVD);
            this.setModalVisible(true, item);
            console.log(item.billingDate);
        };

        // COLOR ORDER CONDITIONAL
        const pedidoSubido =
            <View style={styles.pedidoSubido}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
                <Icon name="ios-home" size={40} color="#77b5fa" />
            </View>;
        const embalaje =
            <View style={styles.embalaje}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
                <Icon name="ios-cube" size={40} color="#f5c15d" />
            </View>;
        const cancelada =
            <View style={styles.cancelada}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
            </View>;
        const produccion =
            <View style={styles.produccion}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
                <Icon  name="ios-settings" size={40} color="#cc3906" />
            </View>;
        const facturado =
            <View style={styles.facturado}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
                <Icon name="ios-paper" size={40} color="#9dc94e" />
            </View>;
        const otroEstado =
            <View style={styles.otroEstado}>
                <Text style={styles.pedidoSubidoText}> {item.TRUORG / 10000} </Text>
            </View>;

        let colorState;
        let nameState;
        // SWITCH LIST
        switch (item.TRDC20.trim()) {
            case 'EN BODEGA PRT':
                //console.log("PEDIDO SUBIDO");
                colorState = pedidoSubido;
                nameState = 'BODEGA';
                break;
            case 'EN EMBALAJE':
                //console.log("EN EMBALAJE");
                colorState = embalaje;
                nameState = 'EMBALAJE';
                break;
            case 'CANCELADA':
                //console.log("CANCELADA");
                colorState = cancelada;
                break;
            case 'EN PRODUCCION':
                //console.log("EN PRODUCCION");
                colorState = produccion;
                nameState = 'PRODUCCIÓN';
                break;
            case 'PROD FACTURADO':
                //console.log("PROD FACTURADO");
                colorState = facturado;
                nameState = 'FACTURADO';
                break;
            default:
                //console.log("OTRO ESTADO");
                colorState = otroEstado;

        }

        return (

            <View>

                <TouchableOpacity style={styles.listContainerGo} onPress={() => handlePress(item)}>
                    <View style={styles.container}>
                        <View style={styles.left}>
                            {colorState}
                        </View>

                        <View style={styles.right}>
                            <Text style={styles.ratingLeft}> {item.TRDSC1}</Text>
                            <Text style={styles.ratingLeftState}> {nameState}</Text>
                        </View>

                    </View>
                </TouchableOpacity>
            </View>

        )
    };


    render() {

        // COLOR ORDER CONDITIONAL
        const pedidoSubidoDetail =
            <View style={styles.pedidoSubidoDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;
        const embalajeDetail =
            <View style={styles.embalajeDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;
        const canceladaDetail =
            <View style={styles.canceladaDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;
        const produccionDetail =
            <View style={styles.produccionDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;
        const facturadoDetail =
            <View style={styles.facturadoDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;
        const otroEstadoDetail =
            <View style={styles.otroEstadoDetail}>
                <Text style={styles.pedidoSubidoTextDetail}> {this.state.itemTRUORG} </Text>
            </View>;

        let colorDetail = (this.state.itemTRDC20);
        let nameStateDetail = (this.state.itemTRDC20);
        //console.log(colorDetail);
        // SWITCH LIST DETAIL
        switch (colorDetail) {
            case 'EN BODEGA PRT':
                //console.log("PEDIDO SUBIDO");
                colorDetail = pedidoSubidoDetail;
                nameStateDetail = 'BODEGA';
                break;
            case 'EN EMBALAJE':
                //console.log("EN EMBALAJE");
                colorDetail = embalajeDetail;
                nameStateDetail = 'EMBALAJE';
                break;
            case 'CANCELADA':
                //console.log("CANCELADA");
                colorDetail = canceladaDetail;
                nameStateDetail = 'CANCELADA';
                break;
            case 'EN PRODUCCION':
                //console.log("EN PRODUCCION");
                colorDetail = produccionDetail;
                nameStateDetail = 'PRODUCCIÓN';
                break;
            case 'PROD FACTURADO':
               //console.log("PROD FACTURADO");
                colorDetail = facturadoDetail;
                nameStateDetail = 'FACTURADO';
                break;
            default:
                //console.log("OTRO ESTADO");
                colorDetail = otroEstadoDetail;
        }

    // Billing Date Show
        const billingNone = <Text> </Text>;
        const billingOk = <Text> {this.state.itemTRIVD} </Text>;

        let billingstate;
        if(this.state.itemTRIVD === "Invalid date"){
            billingstate = billingNone;
        }else{
            billingstate = billingOk;
        }




        return (

            <Layout

            >

                <View style={styles.listaGo}>



                    <ScrollView
                        keyExtractor={this.keyExtractor}
                        data={this.props.list}
                        ListEmptyComponent={this.renderEmpty}
                        ItemSeparatorComponent={this.itemSeparator}
                        renderItem={this.renderItem}
                    />

                </View>

                <View style={{flex: 1}}>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}>
                        <View style={styles.modalView}>

                            <View style={styles.modalViewheader}>
                                <Text style={styles.modalViewheaderText}>
                                    PEDIDO: {this.state.itemTRDOCO}
                                </Text>
                                <View style={styles.modalViewContainerDetailOp}>
                                    {colorDetail}
                                </View>
                            </View>




                            <View style={styles.modalViewContainer}>

                                <View style={styles.modalViewContainerDetail}>
                                    <Text style={styles.modalViewContainerTitle}>Descripción 1: </Text>
                                    <Text style={styles.modalViewContainerText}>{this.state.itemTRDSC1} </Text>
                                </View>
                                <View style={styles.modalViewContainerDetail}>
                                    <Text style={styles.modalViewContainerTitle}>Descripción 2: </Text>
                                    <Text style={styles.modalViewContainerText}>{this.state.itemTRDSC2} </Text>
                                </View>
                                <View style={styles.modalViewContainerDetail}>
                                    <Text style={styles.modalViewContainerTitle}>Estado de Pedido: </Text>
                                    <Text style={styles.modalViewContainerText}>
                                        {nameStateDetail}

                                        {billingstate}

                                    </Text>
                                </View>

                                <View style={styles.modalViewContainerDetail}>
                                    <Text style={styles.modalViewContainerTitle}>Observación: </Text>
                                    <Text style={styles.modalViewContainerText}>{this.state.itemTRVR01}</Text>
                                </View>

                                <View style={styles.modalViewContainerDetailEntrega}>
                                    <Text style={styles.modalViewContainerTitleEntrega}>Entrega Comprometida: </Text>
                                    <Text style={styles.modalViewContainerTextEntrega}>{this.state.itemTRPDDJ} </Text>
                                </View>




                            </View>



                            <View style={styles.closeButtonOrder}>
                                <TouchableOpacity
                                    style={styles.closeButtonOrderTouch}
                                    animationType="slide"
                                    onPress={() => this.changeModalVisibility(false)}>
                                    <Text style={styles.closeButtonOrderTouchText}>
                                        CERRAR
                                    </Text>
                                </TouchableOpacity>
                            </View>





                        </View>

                    </Modal>

                </View>


            </Layout>

        )
    }
}


const styles = StyleSheet.create({
    modalViewContainerDetail: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#8e8f92',
        marginBottom: 7,
        paddingBottom: 7,
    },
    modalViewContainerTitle: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    modalViewContainerText: {
        fontWeight: '400',
        fontSize: 17,
        color: '#5a5b5e',
        paddingTop: 8,
    },
    modalViewContainerDetailEntrega: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#8e8f92',
        marginBottom: 7,
        marginTop: 10,
        padding: 10,
        borderRadius: 4,
        backgroundColor: '#f06c26',

    },
    modalViewContainerTitleEntrega: {
        fontWeight: 'bold',
        fontSize: 14,
        color: '#fff',
        textAlign: 'center'
    },
    modalViewContainerTextEntrega: {
        fontWeight: '400',
        fontSize: 18,
        color: '#fff',
        textAlign: 'center',
    },
    listContainerGo: {
        backgroundColor: '#f4f4f4',
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    containerIco: {
        width: 10,
        paddingStart: 20,
        marginRight: 10,
        alignItems: 'flex-end',
        justifyContent: 'space-between',

    },
    /* MODAL */
    modalView: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#f0f0f0',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        shadowColor: "#000",

    },
    modalViewContainer: {
        alignSelf: 'stretch',
        marginTop: -20,
        padding: 15,
        flex: 2,
        marginLeft: 15,
        marginBottom: 20,
        marginRight: 15,
        borderRadius: 6,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 5,
    },

    left: {
        width: 80,
        justifyContent: 'space-between',
        borderLeftWidth: 1,
        borderLeftColor: '#ccc'
    },
    right: {
        paddingLeft: 10,
        justifyContent: 'space-between',
    },
    title: {
        fontSize: 18,
        color: '#44546b',
    },
    ratingLeft: {
        textAlign: 'left',
        justifyContent: 'center',
        fontWeight: '400',
        paddingBottom: 0

    },
    ratingLeftState: {
        fontWeight: '600',
    },

    /*COLOR LISTA*/
    pedidoSubido: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#77b5fa',
        width: 50,
        height: 50,
        //borderRadius: 50,
        marginLeft: 13,
        //shadowColor: "#77b5fa",
        //shadowOffset: {
        //    width: 0,
        //    height: 3,
        //},
        //shadowOpacity: 0.27,
        //shadowRadius: 4.65,

        //elevation: 6,

    },
    embalaje: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#f5c15d',
        width: 50,
        height: 50,
        //borderRadius: 50,
        marginLeft: 13,
        //shadowColor: "#f5c15d",
        //shadowOffset: {
          //  width: 0,
           // height: 3,
        //},
        //shadowOpacity: 0.27,
        //shadowRadius: 4.65,
        //elevation: 6,

    },
    cancelada: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#cc3906',
        width: 50,
        height: 50,
        borderRadius: 50,
        marginLeft: 13,
        shadowColor: "#cc3906",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 6,


    },
    produccion: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#ec4124',
        width: 50,
        height: 50,
        //borderRadius: 50,
        marginLeft: 13,
        //shadowColor: "#ec4124",
        //shadowOffset: {
        //    width: 0,
        //    height: 3,
        //},
        //shadowOpacity: 0.27,
        //shadowRadius: 4.65,

       // elevation: 6,

    },
    facturado: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#9dc94e',
        width: 50,
        height: 50,
        //borderRadius: 50,
        marginLeft: 13,
        //shadowColor: "#9dc94e",
        //shadowOffset: {
        //    width: 0,
        //    height: 3,
       // },
        //shadowOpacity: 0.27,
        //shadowRadius: 4.65,

       // elevation: 6,

    },
    otroEstado: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ccc',
        width: 50,
        height: 50,
        borderRadius: 50,
        marginLeft: 13,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },

    pedidoSubidoText: {
        color: '#fff',
        position: 'absolute',
        zIndex:10,
        fontSize:11,
        textAlign: 'center',
        top: 0,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 4,
        paddingLeft: 2,
        overflow:"hidden",
        backgroundColor: '#4d4d4d',
        borderRadius: 6,
        right: 0,
    },
    modalViewheader: {
        backgroundColor: '#2d323e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        height: 150,
    },
    modalViewheaderText: {
        color: '#fff',
        fontSize: 22,
        marginRight:25,
        flexDirection:'row'
    },
    closeButtonOrder:{
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop:5,
        marginBottom:30,

    },
    closeButtonOrderTouch:{
        backgroundColor: '#f06c26',
        width: 250,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 40,
        shadowColor: "#f06c26",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    closeButtonOrderTouchText:{
        color:'#fff',
        textAlign: 'center',
        },
    pedidoSubidoTextDetail:{
        color: '#fff',
        fontSize: 18,
    },
    modalViewContainerDetailOp:{
        alignItems: 'flex-end',
    },
    // COLOR DETALLES
    canceladaDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#cc3906',
        borderRadius: 50,
        shadowColor: '#cc3906',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    facturadoDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#9dc94e',
        borderRadius: 50,
        shadowColor: '#9dc94e',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    embalajeDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f5c15d',
        borderRadius: 50,
        shadowColor: '#f5c15d',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    pedidoSubidoDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#77b5fa',
        borderRadius: 50,
        shadowColor: '#77b5fa',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    produccionDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ec4124',
        borderRadius: 50,
        shadowColor: '#ec4124',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    otroEstadoDetail:{
        width: 60,
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ccc',
        borderRadius: 50,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    }
});

export default withNavigation(SuggestionList);
