import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Dimensions,ActivityIndicator} from 'react-native';
import io from "socket.io-client";
import feathers from "@feathersjs/feathers";
import socketio from "@feathersjs/socketio-client";
import SuggestionList from "./SuggestionList";
//import {Icon} from "react-native-elements";
import Icon from 'react-native-vector-icons/Ionicons';
//import validation from "validation";


const {width: WIDTH} = Dimensions.get('window');
const socket = io('https://app.cotopaxi.com.ec/');
const client = feathers();

client.configure(socketio(socket));

const app = feathers();
//const socketio = require('feathers-socketio');

app.configure(socketio({
    pingInterval: 10000,
    pingTimeout: 50000
}));

let orders =[];

export class Pedido extends Component {

    state = {
        loading: false,
        dataOrders: '',
        numOrder: '',
        numClient: '',
    };

    filterResults = (status) => {


        if(!this.state.dataOrders) return;

        let filteredOrders = orders;



        filteredOrders = filteredOrders.filter((order) => {
            return order.TRDC20.trim() === status;
        });

        if(!filteredOrders.length){
            alert ('No hay pedidos en el STATUS seleccionado');
        }

        console.log(filteredOrders);



        //console.log(filteredOrders);
        this.setState({
            dataOrders: filteredOrders
        });



    };

    render() {

        const componentDidMount = () => {
            getByCompanyId();
        };

        const rightPad = (str, amount, char) => {
            string = str || '';
            let pad = '';

            for(let i = 0; i < amount; i++) {
                pad += char;
            }

            return (string + pad).substring(0, pad.length);
        };
        /**
         * Click handler to get all orders by company
         */
        const getByCompanyId = () => {

            const {numOrder} = this.state;
            const {numClient} = this.state;


            if(!numOrder) {
                alert('No existe número de pedido');
                return;
            }

            const query = {

                $or: [
                   {
                        TRAN8: numClient,
                        TRDOCO: numOrder,
                    },
                    {
                        TRAN8: numClient,
                        TRVR02 : rightPad(numOrder,25, ' '), //.padEnd(25) SE IGUALÓ A 25 CARACTERES QUE EXISTEN EN LA BD
                    },
                ]
            };

            this.setState({
                loading: true,
            });



            socket.emit('find', 'orders',  query , (error, data) => {
                console.log('sockete');

                if (error) {
                  alert(error.stack);
                }

                if (data && !data.length) {
                    alert('Error: El Numero de Pedido o Código de Cliente es incorrecto');
                }

                orders = data;

                this.setState({
                    dataOrders: data,
                    loading: false,
                });
            });
        };

        if(this.state.loading){
            return(
                <View style={styles.containerLoading}>
                    <ActivityIndicator size='large' color='#f06c26'/>
                    <Text style={styles.containerLoadingText}>Cargando Pedidos...</Text>
                </View>
            );


        }



        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: 'white',
            }}>


                <View style={styles.containerSearch}>

                    <TextInput
                        value={this.state.numClient}
                        onChangeText={numClient => this.setState({numClient})}
                        //onSubmitEditing={this.refs.numOrder.focus}
                        style={styles.textBox}
                        keyboardType="numeric"
                        placeholder="Código Cliente"
                    />

                    <TextInput
                        value={this.state.numOrder}
                        onChangeText={numOrder => this.setState({numOrder})}
                        //onSubmitEditing={this.refs.numOrder.focus}
                        style={styles.textBox}
                        keyboardType="numeric"
                        placeholder="No. Pedido"
                    />


                </View>
<View>
                <TouchableOpacity
                    style={styles.btnBuscar}
                    animationType="slide"
                    onPress={getByCompanyId}>
                    <Icon name="ios-search" size={20} color="#fff" />
                    <Text
                        style={styles.btnBuscartxt}
                    >Consultar</Text>
                </TouchableOpacity>
</View>




                <View style={styles.containerSuggestion}>
                    <SuggestionList

                        list={this.state.dataOrders}
                    />
                </View>
                <View style={styles.containerFilterIcon}>
                    <TouchableOpacity style={styles.containerFilterTouch} onPress={() => this.filterResults('EN PRODUCCION')}>
                        <Icon  name="ios-settings" size={25} color="#cc3906" />
                        <Text style={styles.containerFilterText}>PRODUCCIÓN</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.containerFilterTouch} onPress={() => this.filterResults('EN EMBALAJE')}>
                        <Icon name="ios-cube" size={25} color="#f5c15d" />
                        <Text style={styles.containerFilterText}>EMBALAJE</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.containerFilterTouch} onPress={() => this.filterResults('EN BODEGA PRT')}>
                        <Icon name="ios-home" size={25} color="#77b5fa" />
                        <Text style={styles.containerFilterText}>BODEGA</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.containerFilterTouch} onPress={() => this.filterResults('PROD FACTURADO')}>
                        <Icon name="ios-paper" size={25} color="#9dc94e" />
                        <Text style={styles.containerFilterText}>FACTURADO</Text>
                    </TouchableOpacity>


                </View>

            </View>
        )
    }
};

const styles = StyleSheet.create({

    containerLoading:{
        flex: 1,
        justifyContent:'center',
        backgroundColor: '#fff',

    },
    containerLoadingText:{
        textAlign: 'center',
        color: '#f06c26',
        paddingTop: 10,
    },
    // FILTER Styles
    containerFilterIcon:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    containerFilterText:{
        color:'#fff',
        fontSize: 10
    },
    containerFilterTouch:{
        flex: 1,
      //  flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2d323e',
        borderTopLeftRadius:6,
        borderTopRightRadius:6,
        paddingTop: 6,
        paddingBottom: 6,
        paddingRight: 2,
        paddingLeft: 2,

    },

    containerSearch: {
        flex: 1,
        //alignContent: 'stretch',
        backgroundColor:'#2d323e',
        flexDirection: 'row',
        justifyContent: 'center',
        // alignItems:'stretch',
    },
    containerSuggestion: {
        flex: 6,
        paddingTop:10,
        justifyContent: 'flex-end',
    },
    textBox: {
        zIndex:10,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 15,
        flexGrow: 0.5,
       // width:140,
        //height:50,
        marginRight: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5,
        elevation: 11,
        fontSize: 16,
    },
    btnBuscar: {
        borderRadius: 5,
        borderColor: '#f06c26',
        backgroundColor: 'rgba(216,114,44,1)',
        justifyContent: 'center',
        alignItems:'center',
        flexDirection:'row',
        marginTop: 15,
        marginBottom: 15,
        paddingTop:15,
        paddingBottom:15,
        marginRight: 15,
        marginLeft: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        padding:10,
        elevation: 5,

    },
    btnBuscartxt: {
        color: '#fff',
        fontSize: 17,
    }

});


export default Pedido;
