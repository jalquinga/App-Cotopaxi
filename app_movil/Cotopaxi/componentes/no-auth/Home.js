import React, {Component} from 'react';
import {Button, Dimensions, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import bgImage from "../../assets/bg.jpg";
import logo from "../../assets/logo.png";

const {width: WIDTH} = Dimensions.get('window');


export class Home extends Component{
    render() {
        //props.navigation.navigate();
        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>

                <View style={styles.logoContainer}>

                    <Image source={logo} style={styles.logo}>

                    </Image>

                </View>

                <TouchableOpacity style={styles.btnLogin}
                                  onPress={() => this.props.navigation.navigate('PedidoScreen')}>

                    <Text
                        style={styles.text}
                        onPress={() => this.props.navigation.navigate('PedidoScreen')}>

                        TRACKING DE PEDIDO

                    </Text>


                </TouchableOpacity>



                <Text
                    style={styles.leyenda}>

                    Consulte el estado de su pedido en tiempo real.

                </Text>
            </ImageBackground>


        )
    }
}

const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center',

        //justifyContent: 'center',
        //alignItems: 'center',

    },
    btnLogin: {
        width: WIDTH - 55,
        height: 60,
        borderRadius: 30,
        borderColor: '#f06c26',
        borderWidth: 1,
        //fontSize:36,
        backgroundColor: 'rgba(216,114,44,1)',
        justifyContent: 'center',
        marginTop: 220,
    },
    text: {
        color: '#fff',
        fontSize: 15,
        textAlign: 'center'
    },

    leyenda: {
        color: '#ccc',
        fontSize: 14,
        textAlign: 'center',
        marginTop: 10
    },

    logoContainer: {
        alignItems: 'center',
    },
    logo: {
        marginTop: 150,
        width: 320,
        height: 100,

    },
    //página B
    textTitle: {
        width: WIDTH - 55,
        color: '#565656',
        fontSize: 20,
        textAlign: 'left',

        marginTop: 15
    },
    textBox: {
        width: WIDTH - 55,
        color: '#565656',
        backgroundColor: '#fff',
        fontSize: 20,
        textAlign: 'left',
        margin: 15,
        paddingLeft: 10,
        height: 50,
        borderRadius: 4,
        borderColor: '#f06c26',
        borderWidth: 1,
        marginTop: 10
    },
    btnBuscar: {
        width: WIDTH - 55,
        height: 60,
        borderRadius: 30,
        borderColor: '#f06c26',
        backgroundColor: 'rgba(216,114,44,1)',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 15
    },

});

export default Home
