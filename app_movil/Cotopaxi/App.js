import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
//Import Views';
import Home from './componentes/no-auth/Home';
import Pedido from './componentes/no-auth/Pedido';

const AppNavigator = createStackNavigator({

    HomeScreen: {
        screen: Home,
        navigationOptions: {
            header: null, //Remove header
        },
    },
    PedidoScreen: { screen: Pedido,
        navigationOptions: {
            title: 'Consulta de pedidos:'
        },
    },


});


export default class App extends Component {
    render() {
        return (
            <AppNavigator />
        );
    }
}
