@extends('layouts.app_auth')

@section('content')


        <div id="wrapper">
            <div class="content-wrapper">
                <div class="content custom-scrollbar" style="height: 100vh">

                    <div id="login-v2" class="row no-gutters">

                        <div class="intro col-12 col-md light-fg">

                            <div class="d-flex flex-column align-items-center align-items-md-start text-center text-md-left py-16 py-md-32 px-12">

                                <div class="logo mb-8">
                                    <img width="100%" src="{{ asset('img/Logo-Cotopaxi-01.png') }}">
                                </div>

                                <div class="title">
                                    Bienvenido a nuestro aplicativo.
                                </div>

                                <div class="description pt-2">
                                    Nuestro sistema le permite hacer un tracking de su pedido, obteniedo el estado del mismo en tiempo real.
                                </div>

                            </div>
                        </div>

                        <div class="form-wrapper col-12 col-md-auto d-flex justify-content-center p-4 p-md-0">

                            <div class="form-content md-elevation-8 h-100 bg-white text-auto py-16 py-md-32 px-12">

                                <div class="title h5">Ingreso al sistema</div>

                                <div class="description mt-2">Introduzca sus credenciales de acceso</div>

                                <div class="">


                                    <div >
                                        <form class="mt-8" method="POST" action="{{ route('login') }}">
                                            @csrf

                                            <div class="form-group mb-4">

                                                <input id="email" type="email"
                                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                       name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="email">Email address</label>

                                            </div>


                                            <div class="form-group mb-4">


                                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                                <label for="password">Password</label>
                                            </div>





                                            <div class="remember-forgot-password row no-gutters align-items-center justify-content-between pt-4">

                                                <div class="form-check remember-me mb-4">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input" aria-label="Remember Me" />
                                                        <span class="checkbox-icon"></span>
                                                        <span class="form-check-description">Recordarme</span>
                                                    </label>
                                                </div>



                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-12 ">
                                                    <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto">
                                                        {{ __('Login') }}
                                                    </button>

                                                    <a class="btn btn-block btn-secondary my-4 mx-auto" href="{{ route('password.request') }}">
                                                        {{ __('Olvidó su contraseña?') }}
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>




                          <!--      <div class="hidden register d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                                    <span class="text mr-sm-2">Don't have an account?</span>
                                    <a class="link text-secondary" href="pages-auth-register-v2.html">Create an account</a>
                                </div> -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

            </div>
        </div>
    </div>
@endsection
