@extends('layouts.app')

@section('content')

    <nav id="toolbar" class="bg-white">

        <div class="row no-gutters align-items-center flex-nowrap">

            <div class="col">


            </div>

            <div class="col-auto">

                <div class="row no-gutters align-items-center justify-content-end">

                    <div class="user-menu-button dropdown">

                        <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4" role="button" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="avatar-wrapper">
                                <img class="avatar" src="{{ asset('css/assets/images/avatars/profile.jpg') }}">

                                <i class="status text-green icon-checkbox-marked-circle s-4"></i>
                            </div>
                            <span class="username mx-3 d-none d-md-block">
                                     @if(Auth::user()->hasRole('admin'))
                                    <div>Administrador</div>
                                @else
                                    <div>Usuario</div>
                                @endif
                                </span>
                        </div>

                        <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">

                            <!--   <a class="dropdown-item" href="#">
                                   <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="icon-account"></i>
                                       <span class="px-3">My Profile</span>
                                   </div>
                               </a>

                               <a class="dropdown-item" href="#">
                                   <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="icon-email"></i>
                                       <span class="px-3">Inbox</span>
                                   </div>
                               </a>

                               <a class="dropdown-item " href="#">
                                   <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="status text-green icon-checkbox-marked-circle"></i>
                                       <span class="px-3">Online</span>
                                   </div>
                               </a> -->

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                                <div class="row no-gutters align-items-center flex-nowrap">
                                    <i class="icon-logout"></i>
                                    <span class="px-3">

                                     {{ __('Logout') }}


                                        </span>

                                </div>
                            </a>



                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>

                    <div class="toolbar-separator"></div>

                </div>
            </div>
        </div>
    </nav>
    <div id="wrapper">
        <aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
            <div class="aside-content bg-primary-700 text-auto">

                <div class="aside-toolbar">

                    <div class="logo">
                        <img width="135px" src="{{ asset('img/Logo-Cotopaxi-02.png') }}">
                    </div>

                    <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block" data-fuse-aside-toggle-fold>
                        <i class="icon icon-backburger"></i>
                    </button>

                </div>

                <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

                    <li class="subheader">
                        <span>HISTORIAL</span>
                    </li>




                    <li class="nav-item" role="tab" id="heading-ecommerce">

                        <a class="nav-link ripple with-arrow " data-toggle="collapse" data-target="#collapse-ecommerce" href="#" aria-expanded="true" aria-controls="collapse-ecommerce">

                            <i class="icon s-4 icon-cart"></i>

                            <span>PEDIDOS</span>
                        </a>
                        <ul id="collapse-ecommerce" class='collapse show' role="tabpanel" aria-labelledby="heading-ecommerce" data-children=".nav-item">



                            <li class="nav-item">
                                <a class="nav-link ripple active" href="#" data-url="apps-e-commerce-orders.html">

                                    <span>Lista de Pedidos</span>
                                </a>
                            </li>

                        </ul>
                    </li>





                </ul>
            </div>

        </aside>
        <div class="content-wrapper">
            <div class="content custom-scrollbar">

                <div id="e-commerce-orders" class="page-layout carded full-width">

                    <div class="top-bg bg-primary"></div>

                    <!-- CONTENT -->
                    <div class="page-content-wrapper">

                        <!-- HEADER -->
                        <div class="page-header light-fg row no-gutters align-items-center justify-content-between">

                            <!-- APP TITLE -->
                            <div class="col-12 col-sm">

                                <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">

                                    <div class="logo-icon mr-3 mt-1">
                                        <i class="icon-cart-outline s-6"></i>
                                    </div>

                                    <div class="logo-text">
                                        <div class="h4">Estado de pedidos</div>
                                        <div class="d-none">Total: 220</div>
                                    </div>

                                </div>
                            </div>
                            <!-- / APP TITLE -->

                            <!-- SEARCH -->
                            <div class="col search-wrapper pl-2">

                                <div class="input-group">

                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-icon">
                                                <i class="icon icon-magnify"></i>
                                            </button>
                                        </span>

                                    <input id="orders-search-input" type="text" class="form-control" placeholder="Search" aria-label="Search" />

                                </div>
                            </div>
                            <!-- / SEARCH -->
                        </div>
                        <!-- / HEADER -->

                        <div class="page-content-card">

                            <table id="e-commerce-orders-table" class="table dataTable">

                                <thead>

                                <tr>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Cod. Cliente</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Cliente</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Documento</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Art&iacute;culo</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Descripci&oacute;n 1</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Descripci&oacute;n 2</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Observacion</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Cantidad</span>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="table-header">
                                            <span class="column-title">Estado</span>
                                        </div>
                                    </th>

                                </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- / CONTENT -->
                </div>

            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="//unpkg.com/@feathersjs/client@^3.0.0/dist/feathers.js"></script>
    <script src="//unpkg.com/socket.io-client@1.7.3/dist/socket.io.js"></script>
    <script type="text/javascript" src="{{ asset('css/assets/js/apps/e-commerce/orders/orders.js') }}"></script>
@endsection
