<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/icons/fuse-icon-font/style.css') }}" >
    <!-- Animate.css -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/node_modules/animate.css/animate.min.css') }}" >
    <!-- PNotify -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/node_modules/pnotify/dist/PNotifyBrightTheme.css') }}" >
    <!-- Nvd3 - D3 Charts -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/node_modules/nvd3/build/nv.d3.min.css') }}"  >
    <!-- Perfect Scrollbar -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css') }}"  >
    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/fuse-html/fuse-html.min.css') }}"  >
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/assets/css/main.css') }}">


    <!-- / STYLESHEETS -->


    <!-- JAVASCRIPT -->
    <!-- jQuery -->

    <!-- Mobile Detect -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/mobile-detect/mobile-detect.min.js') }}"></script>
    <!-- Perfect Scrollbar -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js ') }}"></script>
    <!-- Popper.js -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/popper.js/dist/umd/popper.min.js ') }}" ></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/bootstrap/dist/js/bootstrap.min.js ') }}" ></script>
    <!-- Nvd3 - D3 Charts -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/d3/d3.min.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/nvd3/build/nv.d3.min.js ') }}" ></script>
    <!-- Data tables -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/datatables.net/js/jquery.dataTables.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/datatables-responsive/js/dataTables.responsive.js ') }}" ></script>
    <!-- PNotify -->
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotify.js ') }}" src="../"></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyStyleMaterial.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyButtons.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyCallbacks.j ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyMobile.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyHistory.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyDesktop.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyConfirm.js ') }}" ></script>
    <script type="text/javascript" src="{{ asset('css/assets/node_modules/pnotify/dist/iife/PNotifyReference.js ') }}" ></script>
    <!-- Fuse Html -->
    <script type="text/javascript" src="{{ asset('css/assets/fuse-html/fuse-html.min.js') }}" ></script>
    <!-- Main JS -->
    <script type="text/javascript" src="../assets/js/main.js"></script>
    <!-- / JAVASCRIPT -->

</head>





<body class="layout layout-vertical layout-left-navigation layout-above-toolbar layout-above-footer">



  <div id="app">

        <main class="">
            @yield('content')
        </main>
    </div>
</body>
</html>
