(function ()
{
    const apiUrl = 'http://172.16.2.31:80/';

    // WebSocket init and configuration
    var socket = io(apiUrl);
    var app = feathers();
    app.configure(feathers.socketio(socket));
    app.configure(feathers.authentication());

    $(document).ready(function ()
    {
        $('#e-commerce-orders-table').DataTable(
            {
                dom       : 'rt<"dataTables_footer"ip>',
                columns: [
                    { data: 'TRAN8' },
                    { data: 'TRALPH' },
                    { data: 'TRDOCO' },
                    { data: 'TRLITM' },
                    { data: 'TRDSC1' },
                    { data: 'TRDSC2' },
                    { data: 'TRVR01' },
                    { data: 'TRUORG'},
                    {
                        render: function (data, type, full) {

                            let color= '#ccc';

                            switch (full.TRDC20.trim()) {
                                case 'PEDIDO SUBIDO':
                                    color = '#77b5fa';
                                    break;
                                case 'EN EMBALAJE':
                                    color = '#f5c15d';
                                    break;
                                case 'CANCELADA':
                                    color = '#cc3906';
                                    break;
                                case 'EN PRODUCCION':
                                    color = '#ec4124';
                                    break;
                                case 'PROD FACTURADO':
                                    color = '#9dc94e';
                                    break;
                            }

                            return `<span class="badge badge-info" style="background-color: ${color}">${full.TRDC20}</span>`
                        }
                    },
                ],

                initComplete: function ()
                {
                    var api = this.api(),
                        searchBox = $('#orders-search-input');

                    // Bind an external input as a table wide search box
                    if ( searchBox.length > 0 )
                    {
                        searchBox.on('keyup', function (event)
                        {
                            api.search(event.target.value).draw();
                        });
                    }
                },
                lengthMenu  : [10, 20, 30, 50, 100],
                pageLength  : 10,
                scrollY     : 'auto',
                scrollX     : false,
                responsive  : true,
                autoWidth   : false,

                processing: true,
                serverSide: true,

                ajax: function (data, callback, settings) {

                    let sort = [];
                    sort[data.columns[data.order[0].column].data] = data.order[0].dir === 'asc' ? 1 : -1;

                    // pagination and sorting TODO add pagination to server for this to work
                    const newData = {
                        $limit: data.length,
                        $skip: data.start,
                        $sort: sort
                    };

                    socket.emit('find', 'orders', {}, (error, response) => {
                        if(error) {
                            alert(error.stack);
                        }

                        // map your server's response to the DataTables format and pass it to
                        // DataTables' callback
                        callback({
                            recordsTotal: response.length,
                            recordsFiltered: response.length,
                            data: response
                        });

                    });


                },
            }
        );

    });
})();
